import payload, { Payload } from 'payload';
import type { InitOptions } from 'payload/config';
import { transporter } from './lib/email';

let cached = (global as any).payload;

if (!cached) {
  cached = (global as any).payload = {
    client: null,
    promise: null,
  };
}

interface Args {
  initOptions?: Partial<InitOptions>;
}

export const getPayloadClient = async ({
  initOptions,
}: Args = {}): Promise<Payload> => {
  if (!process.env.PAYLOAD_SECRET) {
    throw new Error('Payload secret is missing');
  }

  if (!process.env.NODEMAILER_FROM_ADDRESS) {
    throw new Error('Missing environment variable NODEMAILER_FROM_ADDRESS');
  }
  if (cached.client) return cached.client;
  if (!cached.promise) {
    cached.promise = payload.init({
      email: {
        transport: transporter,
        fromAddress: process.env.NODEMAILER_FROM_ADDRESS,
        fromName: 'Marketplace Eduardo',
      },
      secret: process.env.PAYLOAD_SECRET,
      local: initOptions?.express ? false : true,
      ...(initOptions || {}),
    });
  }
  try {
    cached.client = await cached.promise;
  } catch (e: unknown) {
    cached.promise = null;
    throw e;
  }

  return cached.client;
};
