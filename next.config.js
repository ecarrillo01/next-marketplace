/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
        remotePatterns: [
            {
                protocol: "http",
                hostname: "localhost",
            }, {
                protocol: 'https',
                hostname: 'shrug.domcloud.dev'
            }
        ],
    },

}

module.exports = nextConfig
